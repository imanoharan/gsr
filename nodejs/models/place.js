module.exports = function(sequelize, DataTypes) {
  return sequelize.define('place',{
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    organization_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
   tablename: 'place',
   freezeTableName: true,
   timestamps: false,
   schema: 'gsr_master'
  });
   place.associate = models => {
    models.place.belongsTo(model.country, {targetKey: 'id', foreignKey: 'country_id'})
}
};
