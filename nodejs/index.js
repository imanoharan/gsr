// index.js

/**

  * Required External Modules
 
  */

const express = require('express');
const path = require('path');
const router = require('express').Router();
const Sequelize = require('sequelize');
//const country = require(path.join(__dirname,'routes','countrylist.js'));
const {Country,Place,Experiment,Study,Organization,Season} = require(path.join(__dirname,'routes','sequelize.js'));
const bodyParser = require('body-parser');


/**

  * App variables

  */

const app = express();
const port = process.env.PORT || "3000";
var country_id_val;
var place_id_val;
var organization_id_val;



/**
 
  * App configuration

  */

app.set("views",path.join(__dirname,"views"));
app.set("view engine","pug");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/*app.use(function (req,res)
{
  var post_data = req.body;
  console.log(post_data); 
}
);*/

//app.use(country);

/**

  * Routes Definitions

  */

 
app.get("/",function(request,response) {
  return response.redirect('/form-with-get-info')
});

app.get("/form-with-get-info",function(request,response) {
  return response.render('form-with-get-info')
})

app.get("/submit-form-with-info",function(request,response){
  var country_name = request.query.Country;
  var study_details = [];


if(country_name){
  
  Study.findAndCountAll({
    required : true,
    include : [
    {model : Place,attributes : [],where : {name : request.query.Location}},
    {model : Country, attributes : [],where : {name : request.query.Country}},
    {model : Organization,attributes :[]},
    {model : Season,attributes :[]},
    {model : Experiment,attributes : [],where : {year : request.query.Year}}
    ],
    attributes: [[Sequelize.col('study.name'),'StudyName'],
    [Sequelize.col('place.name'),'Location'],
    [Sequelize.col('country.name'),'Country'],
    [Sequelize.col('season.name'),'Season'],
    [Sequelize.col('organization.name'),'Organization'],
    [Sequelize.col('experiment.year'),'Year']
    ],
    raw : true,
  }).then(function(result){ 
    //response.json(result.id)
    /*var study_det = {
      'StudyName' : result.id,
       'Location' : result.iso_code,
       'hasc_code' : result.hasc_code,
       'name' : result.name

    }*/

    //study_details.push(result.rows)
    //console.log(study_details)
    //console.log(result.count);
        
          for (var i = 0; i < result.count; i++) {

          // Create an object to save current row's data
          var study_det = {
            'StudyName':result.rows[i].StudyName,
            'Location':result.rows[i].Location,
            'Country':result.rows[i].Country,
            'Season':result.rows[i].Season,
            'Organization':result.rows[i].Organization,
            'Year': result.rows[i].Year
          }
          // Add object into array
          study_details.push(study_det);
      }
    //  console.log(study_details[0].StudyName);
      //response.json(result.rows)
    //country_details.push(country_det);
    //console.log(country_details);
    
     //     console.log(result.dataValues)
    return response.render('countrydet',{"study_details":study_details})
  });

 /* var location_name = request.query.Location;
  
  if(country_id_val){
    Place.findOne({
    where : {name : location_name,country_id : country_id_val
     }
  }).then(function(placeresult){
    place_id_val = placeresult.id;
    organization_id_val = placeresult.organization_id;
  })
}

  if(place_id_val && organization_id_val){
   Study.findAll({
    where : {place_id : place_id_val,country_id : country_id_val,organization_id : organization_id_val
    },
    include: [{
      model: Season,
      attributes:[['name','Season']]
    }],
   attributes:[['name','Experimentname']]
   }).then(function(studyresult){
    response.send(studyresult[0])
   });
 }*/
}
});

 // Handler for error 404 - Resource not found

 app.use((req,res,next) => {
  res.status(404).send('Requested page not found!')
 });
app.use(express.json());
/**

  * Server Activation

  */

app.listen(port,() => {
  console.log('Listening to requests on port ${port}');
});
