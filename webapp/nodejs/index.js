// index.js

/**

  * Required External Modules
 
  */

const express = require('express');
const path = require('path');
const router = require('express').Router();
const Sequelize = require('sequelize');
//const country = require(path.join(__dirname,'routes','countrylist.js'));
const {Country,Place,Experiment,Study,Organization,Season,Content} = require(path.join(__dirname,'routes','sequelize.js'));
const bodyParser = require('body-parser');
const { Op } = require('sequelize');


/**

  * App variables

  */

const app = express();
const port = process.env.PORT || "3000";
var country_id_val;
var place_id_val;
var organization_id_val;



/**
 
  * App configuration

  */

app.set("views",path.join(__dirname,"views"));
app.set("view engine","pug");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

 
app.get("/",function(request,response) {
  return response.redirect('/form-with-get-info')
});

app.get("/form-with-get-info",function(request,response) {
  return response.render('form-with-get-info')
})


app.get("/submit-form-with-info",function(request,response){
  console.log(request.query);
  var country_name = request.query.Country;
  var study_details = [];


if(country_name){
  
  Study.findAndCountAll({
    required : true,
    include : [
    {model : Place,attributes : [],where : {name : request.query.Location}},
    {model : Country, attributes : [],where : {name : request.query.Country}},
    {model : Organization,attributes :[]},
    {model : Season,attributes :[]},
    {model : Experiment,attributes : [],where : {year : request.query.Year}}
    ],
    attributes: [[Sequelize.col('study.name'),'StudyName'],
    [Sequelize.col('place.name'),'Location'],
    [Sequelize.col('country.name'),'Country'],
    [Sequelize.col('season.name'),'Season'],
    [Sequelize.col('organization.name'),'Organization'],
    [Sequelize.col('experiment.year'),'Year']
    ],
    raw : true,
  }).then(function(result){ 

        
          for (var i = 0; i < result.count; i++) {

          // Create an object to save current row's data
          var study_det = {
            'StudyName':result.rows[i].StudyName,
            'Location':result.rows[i].Location,
            'Country':result.rows[i].Country,
            'Season':result.rows[i].Season,
            'Organization':result.rows[i].Organization,
            'Year': result.rows[i].Year
          }
          // Add object into array
          study_details.push(study_det);
      }

    return response.render('countrydet',{"study_details":study_details})
  });


}
});



app.get("/search-with-info",function(request,response){
  var search_string = request.query.Content
  var search_details = [];

console.log("search_string")
console.log(request.query.Content)

if(search_string){
  
  Content.findAndCountAll({
    required : true,
    attributes: [[Sequelize.col('content.file_name'),'Filename'],
    [Sequelize.col('content.file_location'),'Location'],
    [Sequelize.col('content.sheet_name'),'Sheetname'],
    [Sequelize.col('content.file_content'),'Filecontent']
    ],
    where: {
      file_content :  {
        [Op.like]: '%'+request.query.Content+'%'
      }
    },
    raw : true,
  }).then(function(result){ 

        
          for (var i = 0; i < result.count; i++) {

          // Create an object to save current row's data
          var search_det = {
            'Filename':result.rows[i].Filename,
            'Location':result.rows[i].Location,
            'Sheetname':result.rows[i].Sheetname
          }
          // Add object into array
          search_details.push(search_det);
          
      }

    return response.render('searchdet',{"search_details":search_details})
  });

}
});


 // Handler for error 404 - Resource not found

 app.use((req,res,next) => {
  res.status(404).send('Requested page not found!')
 });
app.use(express.json());
/**

  * Server Activation

  */

app.listen(port,() => {
  console.log('Listening to requests on port ${port}');
});
