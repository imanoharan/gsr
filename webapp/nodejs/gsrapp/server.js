const express = require('express')
const app = express()
const path = require('path')
const dotenv = require('dotenv').config({path: path.join(__dirname,'config','.env')});
const pg = require('pg')
const bodyparser = require('body-parser')
const ejs = require('ejs')

// DB Connection String

const connect = "postgres://process.env.username:process.env.password@localhost/process.env.database";

// Assign the view engine to ejs

app.set('view engine','ejs');
app.set('views',__dirname + '/views');

// Set the public folder

app.use(express.static(path.join(__dirname,'public')));

// Body Parser Middleware
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));


app.get('/',function(req,res) {
  res.render('index',{title:"Home"})
})

app.listen(process.env.port_no,function(){
  console.log('Example app listening on port 3000!')
})
