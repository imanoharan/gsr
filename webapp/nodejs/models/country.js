module.exports = function(sequelize, DataTypes) {
  return sequelize.define('country',{
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    iso_code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    hasc_code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
   tablename: 'country',
   freezeTableName: true,
   timestamps: false,
   schema: 'gsr_master'
  });
};
