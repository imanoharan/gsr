module.exports = function(sequelize, DataTypes) {
  return sequelize.define('experiment',{
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    year: {
      type: DataTypes.STRING,
      allowNull: false
    },
    experiment_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    file_location: {
      type: DataTypes.STRING,
      allowNull: false
    },
    file_type: {
       type: DataTypes.STRING,
       allowNull: false
    }
  }, {
   tablename: 'experiment',
   freezeTableName: true,
   timestamps: false,
   schema: 'gsr_operational'
  });
};
