module.exports = function(sequelize, DataTypes) {
  return sequelize.define('organization',{
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    abbrev: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
   tablename: 'organization',
   freezeTableName: true,
   timestamps: false,
   schema: 'gsr_master'
  });
};
