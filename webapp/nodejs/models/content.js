module.exports = function(sequelize, DataTypes) {
  return sequelize.define('content',{
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    file_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    file_location: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sheet_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    file_content: {
       type: DataTypes.STRING,
       allowNull: false
    }
  }, {
   tablename: 'content',
   freezeTableName: true,
   timestamps: false,
   schema: 'gsr_operational'
  });
};

