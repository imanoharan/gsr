module.exports = function(sequelize, DataTypes) {
  return sequelize.define('study',{
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    place_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    country_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    organization_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    name: {
       type: DataTypes.STRING,
       allowNull: false
    },
    season_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
   tablename: 'study',
   freezeTableName: true,
   timestamps: false,
   schema: 'gsr_operational'
  });

  study.associate = models => {
    models.study.belongsTo(model.organization, {targetKey: 'id', foreignKey: 'organization_id'})
}
};
