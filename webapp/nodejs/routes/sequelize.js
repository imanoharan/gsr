const Sequelize = require('sequelize');
const path = require('path');
const CountryModel = require(path.join(__dirname,'..','models','country.js'));
const PlaceModel = require(path.join( __dirname,'..','models','place.js'));
const ExperimentModel = require(path.join(__dirname,'..','models','experiment.js'));
const SeasonModel = require(path.join(__dirname,'..','models','season.js'));
const StudyModel = require(path.join(__dirname,'..','models','study.js'));
const OrganizationModel = require(path.join(__dirname,'..','models','organization.js'));
const ContentModel = require(path.join(__dirname,'..','models','content.js'));


// connection string

const sequelize = new Sequelize('gsr','postgres','postgres', {
  host : 'localhost',
  dialect : 'postgres',
  operatorAliases : false,
  pool : {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

// test the connection to the database

sequelize.authenticate().then(() => {
  console.log('Connection has been established successfully');
})
.catch(err => {
  console.log('Unable to connect to the database:',err);
});

// Import the sequelize data models

const Country = CountryModel(sequelize,Sequelize);
const Place = PlaceModel(sequelize,Sequelize);
const Experiment = ExperimentModel(sequelize,Sequelize);
const Season = SeasonModel(sequelize,Sequelize);
const Study = StudyModel(sequelize,Sequelize);
const Organization = OrganizationModel(sequelize,Sequelize);
const Content = ContentModel(sequelize,Sequelize);

// Create the association of the tables

Place.belongsTo(Country,{targetKey:'id',foreignKey:'country_id'});
Study.belongsTo(Country,{targetKey:'id',foreignKey: 'country_id'});
Study.belongsTo(Place,{targetKey:'id',foreignKey: 'place_id'});
Study.belongsTo(Organization,{targetKey:'id',foreignKey: 'organization_id'});
Study.belongsTo(Season,{targetKey:'id',foreignKey: 'season_id'});
Study.belongsTo(Experiment,{targetKey:'id',foreignKey: 'experiment_id'});
Organization.belongsTo(Place,{targetKey:'id',foreignKey: 'organization_id'});
Place.belongsTo(Organization,{targetKey:'id',foreignKey:'organization_id'});
Country.belongsTo(Place,{targetKey:'id',foreignKey:'country_id'});

module.exports = {Country,Place,Experiment,Season,Study,Organization,Content};
