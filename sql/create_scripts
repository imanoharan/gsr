CREATE SCHEMA gsr_master
  AUTHORIZATION postgres;

COMMENT ON SCHEMA gsr_master
  IS 'More or less correct data that does not change relatively frequently, and are used to maintain data integrity in the entire system';


-- Table: gsr_master.country

-- DROP TABLE gsr_master.country;

CREATE TABLE gsr_master.country
(
  id serial NOT NULL, -- Primary key
  iso_code character varying(3) NOT NULL, -- ISO-3 code of the country
  hasc_code character varying(2) NOT NULL, -- HASC code of the country
  name character varying NOT NULL, -- English name of the country
  remarks text,
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(),
  creator_id integer NOT NULL DEFAULT 1,
  modification_timestamp timestamp without time zone,
  modifier_id integer,
  notes text,
  is_void boolean NOT NULL DEFAULT false,
  event_log jsonb,
  CONSTRAINT country_id_pkey PRIMARY KEY (id),
  CONSTRAINT country_hasc_code_key UNIQUE (hasc_code),
  CONSTRAINT country_iso_code_key UNIQUE (iso_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_master.country
  OWNER TO postgres;

COMMENT ON TABLE gsr_master.country
  IS 'The lookup table for all countries in the world.';
COMMENT ON COLUMN gsr_master.country.id IS 'Primary key';
COMMENT ON COLUMN gsr_master.country.iso_code IS 'ISO-3 code of the country';
COMMENT ON COLUMN gsr_master.country.hasc_code IS 'HASC code of the country';
COMMENT ON COLUMN gsr_master.country.name IS 'English name of the country';


-- Table: gsr_master."user"

-- DROP TABLE gsr_master."user";

CREATE TABLE gsr_master."user"
(
  id serial NOT NULL, -- Primary key of the record in the table
  email character varying(64), -- Active email address of the user. Currently supports IRRI email addresses.
  username character varying(64), -- Unique username of the user taken from email address by removing all characters after and including the '@' sign
  user_type integer NOT NULL, -- Administrator (admin) or non-administrator (non-admin)
  status integer NOT NULL DEFAULT 1, -- Whether the user can log in to the system (active 1) or not (inactive 0)
  last_name character varying(32) NOT NULL, -- Last name or family name of the user
  first_name character varying(32) NOT NULL, -- First or given name of the user
  middle_name character varying(32), -- Middle name of the user
  display_name character varying(64) NOT NULL, -- Name of the user to be displayed in the system
  salutation character varying(16), -- Name to address the user in some parts of the system like the user profile
  valid_start_date date, -- Start date to allow the user to use the system
  valid_end_date date, -- Last date allowed for the user to use the system
  remarks text, -- Additional details about the user
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record was added to the table
  creator_id integer NOT NULL DEFAULT 1, -- ID of the user who added the record to the table
  modification_timestamp timestamp without time zone, -- Timestamp when the record was last modified
  modifier_id integer, -- ID of the user who last modified the record
  notes text, -- Additional details added by an admin; can be technical or advanced details
  is_void boolean NOT NULL DEFAULT false, -- Indicator whether the record is deleted or not
  is_person boolean NOT NULL DEFAULT false, -- Indicates whether a user is a regular person without access to the system or a user with login credentials
  uuid uuid NOT NULL DEFAULT uuid_generate_v4(), -- Universally unique identifier of the record
  document tsvector, -- Sorted list of distinct lexemes which are normalized; used in search query
  record_uuid uuid NOT NULL DEFAULT uuid_generate_v4(), -- Universally unique identifier of the record
  event_log jsonb, -- Changes made to the record
  CONSTRAINT user_id_pkey PRIMARY KEY (id),
  CONSTRAINT user_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table
  CONSTRAINT user_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table
  CONSTRAINT user_email_ukey UNIQUE (email),
  CONSTRAINT user_username_ukey UNIQUE (username),
  CONSTRAINT user_email_username_chk CHECK (is_person = false AND username IS NOT NULL AND email IS NOT NULL),
  CONSTRAINT user_status_chk CHECK (status = ANY (ARRAY[1, 0])),
  CONSTRAINT user_user_type_chk CHECK (user_type = ANY (ARRAY[0, 1]))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_master."user"
  OWNER TO postgres;
COMMENT ON TABLE gsr_master."user"
  IS 'Registered users in the system are granted with roles and are assigned to one or more teams.';
COMMENT ON COLUMN gsr_master."user".id IS 'Primary key of the record in the table';
COMMENT ON COLUMN gsr_master."user".email IS 'Active email address of the user. Currently supports IRRI email addresses.';
COMMENT ON COLUMN gsr_master."user".username IS 'Unique username of the user taken from email address by removing all characters after and including the ''@'' sign';
COMMENT ON COLUMN gsr_master."user".user_type IS 'Administrator (admin) or non-administrator (non-admin)';
COMMENT ON COLUMN gsr_master."user".status IS 'Whether the user can log in to the system (active 1) or not (inactive 0)';
COMMENT ON COLUMN gsr_master."user".last_name IS 'Last name or family name of the user';
COMMENT ON COLUMN gsr_master."user".first_name IS 'First or given name of the user';
COMMENT ON COLUMN gsr_master."user".middle_name IS 'Middle name of the user';
COMMENT ON COLUMN gsr_master."user".display_name IS 'Name of the user to be displayed in the system';
COMMENT ON COLUMN gsr_master."user".salutation IS 'Name to address the user in some parts of the system like the user profile';
COMMENT ON COLUMN gsr_master."user".valid_start_date IS 'Start date to allow the user to use the system';
COMMENT ON COLUMN gsr_master."user".valid_end_date IS 'Last date allowed for the user to use the system';
COMMENT ON COLUMN gsr_master."user".remarks IS 'Additional details about the user';
COMMENT ON COLUMN gsr_master."user".creation_timestamp IS 'Timestamp when the record was added to the table';
COMMENT ON COLUMN gsr_master."user".creator_id IS 'ID of the user who added the record to the table';
COMMENT ON COLUMN gsr_master."user".modification_timestamp IS 'Timestamp when the record was last modified';
COMMENT ON COLUMN gsr_master."user".modifier_id IS 'ID of the user who last modified the record';
COMMENT ON COLUMN gsr_master."user".notes IS 'Additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN gsr_master."user".is_void IS 'Indicator whether the record is deleted or not';
COMMENT ON COLUMN gsr_master."user".is_person IS 'Indicates whether a user is a regular person without access to the system or a user with login credentials';
COMMENT ON COLUMN gsr_master."user".uuid IS 'Universally unique identifier of the record';
COMMENT ON COLUMN gsr_master."user".document IS 'Sorted list of distinct lexemes which are normalized; used in search query';
COMMENT ON COLUMN gsr_master."user".record_uuid IS 'Universally unique identifier of the record';
COMMENT ON COLUMN gsr_master."user".event_log IS 'Changes made to the record';

COMMENT ON CONSTRAINT user_creator_id_fkey ON gsr_master."user" IS 'Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table';
COMMENT ON CONSTRAINT user_modifier_id_fkey ON gsr_master."user" IS 'Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table';

-- Table: gsr_master.organization

-- DROP TABLE gsr_master.organization;

CREATE TABLE gsr_master.organization
(
  id serial NOT NULL, -- Primary key of the record in the table
  abbrev character varying(128) NOT NULL, -- Short name identifier or abbreviation of the organization
  name character varying(255) NOT NULL, -- Name identifier of the organization
  display_name character varying(255), -- Display name of the organization
  description text, -- Description of the organization
  remarks integer, -- Additional details about the record
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record was added to the table
  creator_id integer NOT NULL DEFAULT 1, -- ID of the user who added the record to the table
  modification_timestamp time without time zone, -- Timestamp when the record was last modified
  modifier_id integer, -- ID of the user who last modified the record
  notes text, -- Additional details added by an admin; can be technical or advanced details
  is_void boolean NOT NULL DEFAULT false, -- Indicator whether the record is deleted or not
  CONSTRAINT organization_id_pkey PRIMARY KEY (id),
  CONSTRAINT organization_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT organization_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_master.organization
  OWNER TO postgres;
COMMENT ON TABLE gsr_master.organization
  IS 'List of organizations';
COMMENT ON COLUMN gsr_master.organization.id IS 'Primary key of the record in the table';
COMMENT ON COLUMN gsr_master.organization.abbrev IS 'Short name identifier or abbreviation of the organization';
COMMENT ON COLUMN gsr_master.organization.name IS 'Name identifier of the organization';
COMMENT ON COLUMN gsr_master.organization.display_name IS 'Display name of the organization';
COMMENT ON COLUMN gsr_master.organization.description IS 'Description of the organization';
COMMENT ON COLUMN gsr_master.organization.remarks IS 'Additional details about the record';
COMMENT ON COLUMN gsr_master.organization.creation_timestamp IS 'Timestamp when the record was added to the table';
COMMENT ON COLUMN gsr_master.organization.creator_id IS 'ID of the user who added the record to the table';
COMMENT ON COLUMN gsr_master.organization.modification_timestamp IS 'Timestamp when the record was last modified';
COMMENT ON COLUMN gsr_master.organization.modifier_id IS 'ID of the user who last modified the record';
COMMENT ON COLUMN gsr_master.organization.notes IS 'Additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN gsr_master.organization.is_void IS 'Indicator whether the record is deleted or not';


-- Index: gsr_master.organization_abbrev_idx

-- DROP INDEX gsr_master.organization_abbrev_idx;

CREATE UNIQUE INDEX organization_abbrev_idx
  ON gsr_master.organization
  USING btree
  (abbrev COLLATE pg_catalog."default");



-- Table: gsr_master.place

-- DROP TABLE gsr_master.place;

CREATE TABLE gsr_master.place
(
  id serial NOT NULL, -- Primary key of the record in the table
  name character varying(256) NOT NULL, -- Name identifier of the place
  description text, -- Description about the place
  display_name character varying(256) NOT NULL, -- Name of the place to show to users
  remarks text, -- Additional details of the place
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record was added to the table
  creator_id integer NOT NULL DEFAULT 1, -- ID of the user who added the record to the table
  modification_timestamp timestamp without time zone, -- Timestamp when the record was last modified
  modifier_id integer, -- ID of the user who last modified the record
  notes text, -- Additional details added by an admin; can be technical or advanced details
  is_void boolean NOT NULL DEFAULT false, -- Indicator whether the record is deleted or not
  organization_id integer,
  is_active boolean NOT NULL DEFAULT true,
  country_id integer,
  rank double precision, -- Rank of the record in a list
  event_log jsonb,
  CONSTRAINT place_id_pkey PRIMARY KEY (id),
  CONSTRAINT place_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table
  CONSTRAINT place_country_id FOREIGN KEY (country_id)
      REFERENCES gsr_master.country (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT place_organization_id FOREIGN KEY (organization_id)
      REFERENCES gsr_master.organization (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT place_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE -- Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table
    )
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_master.place
  OWNER TO postgres;
COMMENT ON TABLE gsr_master.place
  IS 'Represents the physical places and their segmentations such as geographies, locations, areas, zones, etc.
  - Breeding hub
  - Breeding location
  - Facility
  - Field
  - Farm
  - Glasshouse
  - Warehouse
  - Storage';
COMMENT ON COLUMN gsr_master.place.id IS 'Primary key of the record in the table';
COMMENT ON COLUMN gsr_master.place.name IS 'Name identifier of the place';
COMMENT ON COLUMN gsr_master.place.description IS 'Description about the place';
COMMENT ON COLUMN gsr_master.place.display_name IS 'Name of the place to show to users';
COMMENT ON COLUMN gsr_master.place.remarks IS 'Additional details of the place';
COMMENT ON COLUMN gsr_master.place.creation_timestamp IS 'Timestamp when the record was added to the table';
COMMENT ON COLUMN gsr_master.place.creator_id IS 'ID of the user who added the record to the table';
COMMENT ON COLUMN gsr_master.place.modification_timestamp IS 'Timestamp when the record was last modified';
COMMENT ON COLUMN gsr_master.place.modifier_id IS 'ID of the user who last modified the record';
COMMENT ON COLUMN gsr_master.place.notes IS 'Additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN gsr_master.place.is_void IS 'Indicator whether the record is deleted or not';
COMMENT ON COLUMN gsr_master.place.rank IS 'Rank of the record in a list';

COMMENT ON CONSTRAINT place_creator_id_fkey ON gsr_master.place IS 'Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table';
COMMENT ON CONSTRAINT place_modifier_id_fkey ON gsr_master.place IS 'Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table';


-- Index: gsr_master.place_is_void_idx

-- DROP INDEX gsr_master.place_is_void_idx;

CREATE INDEX place_is_void_idx
  ON gsr_master.place
  USING btree
  (is_void);
COMMENT ON INDEX gsr_master.place_is_void_idx
  IS 'Index for the is_void column';

ALTER TABLE gsr_master.place
  ADD CONSTRAINT place_ukey UNIQUE(name,organization_id,country_id);

-- Table: gsr_master.season

-- DROP TABLE gsr_master.season;

CREATE TABLE gsr_master.season
(
  id serial NOT NULL, -- Primary key of the record in the table
  abbrev character varying(128) NOT NULL, -- Short name identifier or abbreviation of the season
  name character varying(256) NOT NULL, -- Name identifier of the season
  description text, -- Description about the season
  display_name character varying(256), -- Name of the season to show to users
  remarks text, -- Additional details about the season
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record was added to the table
  creator_id integer NOT NULL DEFAULT 1, -- ID of the user who added the record to the table
  modification_timestamp timestamp without time zone, -- Timestamp when the record was last modified
  modifier_id integer, -- ID of the user who last modified the record
  notes text, -- Additional details added by an admin; can be technical or advanced details
  is_void boolean NOT NULL DEFAULT false, -- Indicator whether the record is deleted or not
  event_log jsonb,
  CONSTRAINT season_id_pkey PRIMARY KEY (id),
  CONSTRAINT season_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table
  CONSTRAINT season_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table
  CONSTRAINT season_id_chk CHECK (id >= 11 AND id <= 99)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_master.season
  OWNER TO postgres;
COMMENT ON TABLE gsr_master.season
  IS 'List of seasons';
COMMENT ON COLUMN gsr_master.season.id IS 'Primary key of the record in the table';
COMMENT ON COLUMN gsr_master.season.abbrev IS 'Short name identifier or abbreviation of the season';
COMMENT ON COLUMN gsr_master.season.name IS 'Name identifier of the season';
COMMENT ON COLUMN gsr_master.season.description IS 'Description about the season';
COMMENT ON COLUMN gsr_master.season.display_name IS 'Name of the season to show to users';
COMMENT ON COLUMN gsr_master.season.remarks IS 'Additional details about the season';
COMMENT ON COLUMN gsr_master.season.creation_timestamp IS 'Timestamp when the record was added to the table';
COMMENT ON COLUMN gsr_master.season.creator_id IS 'ID of the user who added the record to the table';
COMMENT ON COLUMN gsr_master.season.modification_timestamp IS 'Timestamp when the record was last modified';
COMMENT ON COLUMN gsr_master.season.modifier_id IS 'ID of the user who last modified the record';
COMMENT ON COLUMN gsr_master.season.notes IS 'Additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN gsr_master.season.is_void IS 'Indicator whether the record is deleted or not';

COMMENT ON CONSTRAINT season_creator_id_fkey ON gsr_master.season IS 'Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table';
COMMENT ON CONSTRAINT season_modifier_id_fkey ON gsr_master.season IS 'Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table';


-- Index: gsr_master.season_abbrev_idx

-- DROP INDEX gsr_master.season_abbrev_idx;

CREATE UNIQUE INDEX season_abbrev_idx
  ON gsr_master.season
  USING btree
  (abbrev COLLATE pg_catalog."default");
COMMENT ON INDEX gsr_master.season_abbrev_idx
  IS 'Unique index for the abbrev column';

-- Index: gsr_master.season_is_void_idx

-- DROP INDEX gsr_master.season_is_void_idx;

CREATE INDEX season_is_void_idx
  ON gsr_master.season
  USING btree
  (is_void);
COMMENT ON INDEX gsr_master.season_is_void_idx
  IS 'Index for the is_void column';


-- Trigger: season_event_log_tgr on gsr_master.season

-- DROP TRIGGER season_event_log_tgr ON gsr_master.season;

CREATE TRIGGER season_event_log_tgr
  BEFORE INSERT OR UPDATE
  ON gsr_master.season
  FOR EACH ROW
  EXECUTE PROCEDURE z_admin.log_record_event();


-- Schema: gsr_operational

-- DROP SCHEMA gsr_operational;

CREATE SCHEMA gsr_operational
  AUTHORIZATION postgres;

COMMENT ON SCHEMA gsr_operational
  IS 'Studies, entries, plot and subplot data, study metadata, observation data';

CREATE TABLE gsr_operational.experiment
(
  id serial NOT NULL, --identifier of the record within the table
  experiment_name character varying(250) NOT NULL, --name of the experiment
  year character varying(128) NOT NULL, -- refers to the year when the experiment was conducted
  file_type character varying(128) NOT NULL, --type of the file .xls,.xlsx
  file_location character varying(300) NOT NULL, --the url location of the file will be stored
  remarks text, -- Additional details about the record
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record was added to the table
  creator_id integer NOT NULL DEFAULT 1, -- ID of the user who added the record to the table
  modification_timestamp timestamp without time zone, -- Timestamp when the record was last modified
  modifier_id integer, -- ID of the user who last modified the record
  notes text, -- Additional details added by an admin; can be technical or advanced details
  is_void boolean NOT NULL DEFAULT false, -- Indicator whether the record is deleted (true) or not (false)
  CONSTRAINT experiment_id_pkey PRIMARY KEY (id),
  CONSTRAINT experiment_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION,
  CONSTRAINT experiment_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_operational.experiment
  OWNER TO postgres;

CREATE UNIQUE INDEX experiment_name_idx
  ON gsr_operational.experiment
  USING btree
  (experiment_name COLLATE pg_catalog."default");
COMMENT ON INDEX gsr_operational.experiment_name_idx
  IS 'Unique index for the name column';

-- Table: gsr_operational.study

--DROP TABLE gsr_operational.study;

CREATE TABLE gsr_operational.study
(
  id serial NOT NULL, -- Primary key of the record in the table
  place_id integer NOT NULL, -- Place where the study is conducted
  country_id integer NOT NULL, -- Country where the study is conducted
  organization_id integer NOT NULL, -- Organization where the study is conducted
  season_id integer NOT NULL, -- Season of the place when the study is conducted
  name character varying(256) NOT NULL, -- Name of the study
  title character varying, -- Title of the study; custom name of the study set by the creator
  remarks text, -- Additional details about the study
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record was added to the table
  creator_id integer NOT NULL DEFAULT 1, -- ID of the user who added the record to the table
  modification_timestamp timestamp without time zone, -- Timestamp when the record was last modified
  modifier_id integer, -- ID of the user who last modified the record
  notes text, -- Additional details added by an admin; can be technical or advanced details
  is_void boolean NOT NULL DEFAULT false, -- Indicator whether the record is deleted (true) or not (false)
  is_active boolean NOT NULL DEFAULT true, -- Indicator whethere the study is active (true) or not (false)
  is_draft boolean NOT NULL DEFAULT true, -- Indicator whether the study is still a draft (true) or not (false)
  is_imported boolean DEFAULT false, -- Indicator to distinguish imported studies from studies created in system
  is_uploaded boolean, -- Indicator whether the study is uploaded to the gsr_operational database or not
  study_status character varying(128), -- Indicator of the status of a study: created, imported, upload, active, ...
  experiment_id integer, -- ID of the experiment where the study is a member
  CONSTRAINT study_id_pkey PRIMARY KEY (id),
  CONSTRAINT study_country_id_fkey FOREIGN KEY (country_id)
      REFERENCES gsr_master.country (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT study_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table
  CONSTRAINT study_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES gsr_master."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table
  CONSTRAINT study_organization_id_fkey FOREIGN KEY (organization_id)
      REFERENCES gsr_master.organization (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT study_season_id_fkey FOREIGN KEY (season_id)
      REFERENCES gsr_master.season (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT study_name_ukey UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gsr_operational.study
  OWNER TO postgres;
COMMENT ON TABLE gsr_operational.study
  IS 'Studies';
COMMENT ON COLUMN gsr_operational.study.id IS 'Primary key of the record in the table';
COMMENT ON COLUMN gsr_operational.study.place_id IS 'Place where the study is conducted';
COMMENT ON COLUMN gsr_operational.study.country_id IS 'Country where the study is conducted';
COMMENT ON COLUMN gsr_operational.study.organization_id IS 'Organization where the study is conducted';
COMMENT ON COLUMN gsr_operational.study.season_id IS 'Season of the place when the study is conducted';
COMMENT ON COLUMN gsr_operational.study.name IS 'Name of the study';
COMMENT ON COLUMN gsr_operational.study.title IS 'Title of the study; custom name of the study set by the creator';
COMMENT ON COLUMN gsr_operational.study.remarks IS 'Additional details about the study';
COMMENT ON COLUMN gsr_operational.study.creation_timestamp IS 'Timestamp when the record was added to the table';
COMMENT ON COLUMN gsr_operational.study.creator_id IS 'ID of the user who added the record to the table';
COMMENT ON COLUMN gsr_operational.study.modification_timestamp IS 'Timestamp when the record was last modified';
COMMENT ON COLUMN gsr_operational.study.modifier_id IS 'ID of the user who last modified the record';
COMMENT ON COLUMN gsr_operational.study.notes IS 'Additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN gsr_operational.study.is_void IS 'Indicator whether the record is deleted (true) or not (false)';
COMMENT ON COLUMN gsr_operational.study.is_active IS 'Indicator whethere the study is active (true) or not (false)';
COMMENT ON COLUMN gsr_operational.study.is_draft IS 'Indicator whether the study is still a draft (true) or not (false)';
COMMENT ON COLUMN gsr_operational.study.is_imported IS 'Indicator to distinguish imported studies from studies created in system';
COMMENT ON COLUMN gsr_operational.study.is_uploaded IS 'Indicator whether the study is uploaded to the gsr_operational database or not';
COMMENT ON COLUMN gsr_operational.study.study_status IS 'Indicator of the status of a study: created, imported, upload, active, ...';
COMMENT ON COLUMN gsr_operational.study.experiment_id IS 'ID of the experiment where the study is a member';

COMMENT ON CONSTRAINT study_creator_id_fkey ON gsr_operational.study IS 'Foreign key constraint for the creator_id column, which refers to the id column of gsr_master.user table';
COMMENT ON CONSTRAINT study_modifier_id_fkey ON gsr_operational.study IS 'Foreign key constraint for the modifier_id column, which refers to the id column of the gsr_master.user table';


-- Index: gsr_operational.study_country_id_idx

-- DROP INDEX gsr_operational.study_country_id_idx;

CREATE INDEX study_country_id_idx
  ON gsr_operational.study
  USING btree
  (country_id);

-- Index: gsr_operational.study_is_void_idx

-- DROP INDEX gsr_operational.study_is_void_idx;

CREATE INDEX study_is_void_idx
  ON gsr_operational.study
  USING btree
  (is_void);
COMMENT ON INDEX gsr_operational.study_is_void_idx
  IS 'Index for the is_void column';

-- Index: gsr_operational.study_name_idx

-- DROP INDEX gsr_operational.study_name_idx;

CREATE INDEX study_name_idx
  ON gsr_operational.study
  USING btree
  (name COLLATE pg_catalog."default");

-- Index: gsr_operational.study_place_id_idx

-- DROP INDEX gsr_operational.study_place_id_idx;

CREATE INDEX study_place_id_idx
  ON gsr_operational.study
  USING btree
  (place_id);

-- Index: gsr_operational.study_season_id_idx

-- DROP INDEX gsr_operational.study_season_id_idx;

CREATE INDEX study_season_id_idx
  ON gsr_operational.study
  USING btree
  (season_id);

-- Index: gsr_operational.study_study_status_idx

-- DROP INDEX gsr_operational.study_study_status_idx;

CREATE INDEX study_study_status_idx
  ON gsr_operational.study
  USING btree
  (study_status COLLATE pg_catalog."default");































