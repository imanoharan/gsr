#!/usr/bin/python
import csv
import psycopg2
import pandas as pd
from config import config
 
def metadata():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
      
        # create a cursor
        cur = conn.cursor()
        
   # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
 
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        data = pd.read_csv("output.csv",sep = ',')
        file_name = data['Filename'].values
        file_type = data['Filetype'].values
        sheet_info = data['Sheetinfo'].values
        file_location = data['Location'].values
        #print(sheet_info)
        for i in range(len(file_name)):
            i_file_name = file_name[i]
            i_file_type = file_type[i]
            i_sheet_info = sheet_info[i]
            i_file_location = file_location[i]
            print(sheet_info[i])
            try:
               insert_exp_query = "insert into gsr_operational.metadata(file_name,file_type,sheet_name,file_location) values (%s,%s,%s,%s)"
               cur.execute(insert_exp_query,(i_file_name,i_file_type,i_sheet_info,i_file_location))
            except(Exception,psycopg2.Error) as error:
               print(error)
            conn.commit()
            pass

       
       # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
 
 
if __name__ == '__main__':
    metadata()
