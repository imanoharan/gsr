from tika import config,parser,detector
import os
import csv
import pandas as pd
import xlrd
import json
from bs4 import BeautifulSoup
from pathlib import Path

i_filename = []
i_file_loc = []
i_file_type = []
i_sheet_names_fnl = []
i_filename_fnl = []
i_filename_err = []

directory = '/home/indra/Downloads/contenttestfolder'
empty_sheet_names = 'NA'

def main():

    for root, dirs, files in os.walk(directory):
        for filename in files:
            try:
              i_sheet_names = []
              i_filename.append(filename)
              #i_file_loc.append(root)
              filename_path = (root+'/'+filename)
              file_data = parser.from_file(filename_path,xmlContent=True)
              file_data1 = parser.from_file(filename_path)
              text = file_data['metadata']
              text1=file_data1['content']
              content_text = text1
              #print(len(content_text))
              soup = BeautifulSoup(text1,"html.parser")
              h1text = soup.find_all('h1')
              filetype = Path(filename_path).suffix
              #i_file_type.append(filetype)                
              if ((filetype == '.xls') or (filetype == '.xlsx') or (filetype == '.ods')) :
                 for i in range(len(h1text)):
                   h1text1 = soup.find_all('h1')[i].text
                   i_sheet_names.append(h1text1)
                 i_sheet_names_fnl.append(i_sheet_names)
              else :
                 i_sheet_names_fnl.append(empty_sheet_names)
          
            except Exception as e:
              i_filename_err.append(filename)
              print(filename,e)
              continue
            else:
              i_file_loc.append(root)
              i_file_type.append(filetype)
              i_filename_fnl.append(filename)
              #print(i_filename_fnl,i_file_loc,i_file_type,i_sheet_names_fnl)
         

    df = pd.DataFrame({'Filename': i_filename_fnl,'Filetype' : i_file_type,'Location' : i_file_loc,'Sheetinfo' : i_sheet_names_fnl})
    df.to_csv("output.csv") 

main()
                 
