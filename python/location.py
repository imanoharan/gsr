#!/usr/bin/python
import csv
import psycopg2
import pandas as pd
from config import config
 
def location():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
      
        # create a cursor
        cur = conn.cursor()
        
   # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
 
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        data = pd.read_csv("place.csv",sep = ',')
        country_val = data['Country'].values
        organization_val = data['Organization'].values
        location_val = data['Location'].values
        for i in range(len(country_val)):
            i_name_val = country_val[i]
            select_country_query="select id from gsr_master.country where name in (%s)"
            cur.execute(select_country_query,(i_name_val,))
            i_name = cur.fetchone()
            i_organ_val = organization_val[i]
            select_organ_query = "select id from gsr_master.organization where abbrev in (%s)"
            cur.execute(select_organ_query,(i_organ_val,))
            i_organization = cur.fetchone()
            i_location = location_val[i]
            print(i_name,i_organization,i_location)
            try:
               insert_place_query = "insert into gsr_master.place (name,display_name,country_id,organization_id) values(%s,%s,%s,%s)"
               cur.execute(insert_place_query,(i_location,i_location,i_name,i_organization,))
            except (Exception,psycopg2.Error) as error:
               print(error)
            conn.commit()
            pass
       
       # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
 
 
if __name__ == '__main__':
    location()
