import os
import csv
import pandas as pd
import xlrd
from pathlib import Path
import psycopg2
from config import config

i_sheet_names = []

def main():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
       # read connection parameters
      params = config()
 
       # connect to the PostgreSQL server
      print('Connecting to the PostgreSQL database...')
      conn = psycopg2.connect(**params)
      
       # create a cursor
      cur = conn.cursor()
        
       # execute a statement
      print('PostgreSQL database version:')
      selectstmt  = "SELECT file_name,file_location from gsr_operational.metadata where file_type in %s"
      data = ('.xls','.xlsx','.csv')
      cur.execute(selectstmt,(data,))
 
       # display the PostgreSQL database server version
      file_contents = cur.fetchall()
      print(file_contents)
      
      for row in file_contents:
        file_name = row[0]
        file_location = row[1]
        print(file_name,file_location)
               
        filename_path = (file_location+'/'+file_name)
        wb = xlrd.open_workbook(filename_path)
        i_sheet_names = wb.sheet_names()
        sheet_len = len(i_sheet_names)
        for i in range(sheet_len):
          df = pd.read_excel(filename_path,sheet_name=i_sheet_names[i])
          print(i_sheet_names[i])
          sheet_content = df.to_csv(index=None)
          try:
               insert_exp_query = "insert into gsr_operational.content(file_name,file_location,sheet_name,file_content) values (%s,%s,%s,%s)"
               cur.execute(insert_exp_query,(file_name,file_location,i_sheet_names[i],sheet_content))
          except(Exception,psycopg2.Error) as error:
               print(error)
          conn.commit()
          pass

      cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

main()
                 
