#!/usr/bin/python
import csv
import psycopg2
import pandas as pd
from config import config
 
def study():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
      
        # create a cursor
        cur = conn.cursor()
        
   # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
 
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        data = pd.read_csv("gsrsummary_load1.csv",sep = ',')
        exp_name = data['File Name'].values
        location_name = data['Location'].values
        country_name = data['Country'].values
        organization_name = data['Organization'].values
        season_name = data['Season'].values
        for i in range(len(data)):
            i_exp_name = exp_name[i]
            i_location_name = location_name[i]
            i_country_name = country_name[i]
            i_organization_name = organization_name[i]
            i_season_name = season_name[i]
            try:
               select_country_id = "select id from gsr_master.country where name in (%s)"
               cur.execute(select_country_id,(i_country_name,))
               i_country_id = cur.fetchone()
               
               select_organization_id = "select id from gsr_master.organization where abbrev in (%s)"
               cur.execute(select_organization_id,(i_organization_name,))
               i_organization_id = cur.fetchone()

               select_place_id = "select id from gsr_master.place where country_id = %s and organization_id = %s  and name in (%s)"
               cur.execute(select_place_id,(i_country_id,i_organization_id,i_location_name,))
               i_place_id = cur.fetchone()
              
               select_season_id = "select id from gsr_master.season where abbrev in (%s)"
               cur.execute(select_season_id,(i_season_name,))
               i_season_id = cur.fetchone()

               select_exp_id = "select id,experiment_name from gsr_operational.experiment where experiment_name in (%s)"
               cur.execute(select_exp_id,(i_exp_name,))
               i_exp_id = cur.fetchone()[0]


               insert_study_query = "insert into gsr_operational.study(place_id,country_id,organization_id,season_id,experiment_id,name)values(%s,%s,%s,%s,%s,%s)"
               cur.execute(insert_study_query,(i_place_id,i_country_id,i_organization_id,i_season_id,i_exp_id,i_exp_name,))


            except(Exception,psycopg2.Error) as error:
               print(error)
            conn.commit()
            pass

       
       # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
 
 
if __name__ == '__main__':
    study()
