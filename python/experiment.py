#!/usr/bin/python
import csv
import psycopg2
import pandas as pd
from config import config
 
def experiment():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
      
        # create a cursor
        cur = conn.cursor()
        
   # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
 
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        data = pd.read_csv("gsrsummary_load1.csv",sep = ',')
        exp_name = data['File Name'].values
        exp_file_type = data['File Type'].values
        exp_year = data['Year'].values
        exp_fol_location = data['Folder Location'].values
        for i in range(len(exp_name)):
            i_exp_name = exp_name[i]
            i_exp_file_type = exp_file_type[i]
            i_exp_year = exp_year[i]
            i_exp_fol_location = exp_fol_location[i]
            try:
               insert_exp_query = "insert into gsr_operational.experiment(experiment_name,year,file_type,file_location) values (%s,%s,%s,%s)"
               cur.execute(insert_exp_query,(i_exp_name,i_exp_year,i_exp_file_type,i_exp_fol_location))
            except(Exception,psycopg2.Error) as error:
               print(error)
            conn.commit()
            pass

       
       # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
 
 
if __name__ == '__main__':
    experiment()
